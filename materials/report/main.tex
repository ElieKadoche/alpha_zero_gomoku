\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[english]{babel}
\usepackage[top=2cm,bottom=2cm,left=2cm,right=2cm]{geometry}
\usepackage[default]{sourcesanspro}
\usepackage[hidelinks]{hyperref}
\usepackage[acronym]{glossaries}
\usepackage{glossary-inline}
\usepackage{biblatex}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{multicol}
\usepackage{enumitem}
\usepackage{bm}

\makeglossaries
\input{glossary}
\glossarystyle{inline}
\addbibresource{main.bib}

\newcommand{\itemb}{\item[\( \bullet \)]}

\title{
	\vspace{-25pt}
    \href{https://gitlab.com/ElieKadoche/alpha_zero_gomoku.git}{\includegraphics[scale=0.07]{../logo.png}} \\
    AlphaZero for the game of Gomoku
}
\author{Elie \textsc{Kadoche} - 30/03/2020}
\date{}

\begin{document}
\maketitle

\subsection*{Abstract}
In March 2016, Lee Seedol, one of the greatest Go player of our century lost 4 games on 5 against a new kind of machine. The upheavals that this brings are as great in the world of Go as they are in the world of \gls{ai}. Fascinated by the game, I then decided to totally invest myself in understanding this fabulous machine. After hard work and the opportunity to be part of the \gls{iasd} \href{https://www.lamsade.dauphine.fr/wp/iasd/en/}{master}, I have gathered the necessary knowledge to finally understand this machine and try to rebuild it from scratch.

\section{Introduction}
DeepMind, an \gls{ai} company acquired by Google in 2014 has created in 2018 a fabulous machine, \gls{az} \cite{alphazero}. Today, the games of Go, chess and Shogi and many others undergo important revolutions. \gls{az} achieves "superhuman performance", giving us the opportunity to better understand these games and to discover strategies, tactics, tricks we did not see before.\\

The game of Go was the first to be involved with \gls{ag} \cite{alphago} in 2016 and with \gls{agz} \cite{alphago_zero} in 2017. The methods used by \gls{agz} and \gls{az} are practically identical, differing only on certain points. To fully understand \gls{az}, it is then recommended to read both articles \cite{alphago_zero} and \cite{alphazero} in this order.\\

This project aims at reproducing \gls{az} for the game of Gomoku. Goals are having a simple program with the bare configuration for simplicity and having a well optimized program using C++ and Python for efficient and fast computations. Here we describe what has been done in this project. Technical reminders will be brief since original papers explained in detail how both machines work. Please read \verb|README.md| for installation and usage instructions.\\

In this project I have also implemented the \gls{uct} algorithm, because I think it is a good start for understanding how a \gls{mcts} works. The architecture of \gls{az} is inspired from the \gls{uct} one. Please refer to this paper \cite{uct} for more information on \gls{uct}.

\section{Game of Gomoku}
Gomoku, or Five in a Row is played with Go stones on a Go board. Players alternate turns playing a stone of their color on an empty intersection. The winner is the first player to get an unbroken row of 5 stones horizontally, vertically or diagonally. Black always begins. Even if this game is already solved, it represents a good example to understand how \gls{az} works.

\paragraph{Implementation}
Game, engines, algorithms, etc., implementation can be found in the \verb|cpp| folder. Since the algorithms used are computationally expensive, I decided to code it with C++. A \gls{cli} has also been implemented to see an ongoing game. Players are represented by -1 for black and 1 for white. The winner is either: -1 if black wins, 1 if white wins, 0 if the game is not finished and 2 if it is a draw.

\paragraph{Board representation}
The board at a time \(t\) is represented by 2 matrices \([X_t Y_t]\) of size \((\text{board size, board size})\). For \(i\) rows and \(j\) columns, \(X_{ij}\) and \(Y_{ij}\) correspond to the board intersection \((i, j)\). We call "current player" the player who has to play and "opponent player" the other one. Values for each matrix are the following.

\vspace{-20pt}
\begin{multicols}{2}
	\[X_{ij} = \begin{cases}
	1 \ \text{if current player has a stone in} \ (i,j) \\
	0 \ \text{otherwise}
	\end{cases}\]

	\[Y_{ij} = \begin{cases}
	1 \ \text{if opponent player has a stone in} \ (i,j) \\
	0 \ \text{otherwise}
	\end{cases}\]
\end{multicols}

\paragraph{Input tensor}
Now the board representation is explained, we can describe what a game state representation is at a time \(t\). It is a tensor of size \( (H * 2 + 1, \text{board size, board size})\) and it will be the input of the \gls{az} neural network. Such a tensor is composed as follow: \([X_{t} Y_{t} X_{t-1} Y_{t-1} \dotsc X_{t-H} Y_{t-H} C]\). The value \(H\) represents the history length we decide to keep and \(C\) is a matrix full of 1 if it is black to play, 0 otherwise. "The board is oriented to the perspective of the current player", so at each turn, we need to swap all \(X\) and \(Y\). For time-steps less than 1, \(X\) and \(Y\) are full of 0.

\section{\gls{az} components}
\subsection{Variables}
\label{subsection:variables}

The \gls{az} algorithm is basically a \gls{mcts} guided by an artificial neural network. We have the following information for each node (or game state) \(s\) of the tree, kept in memory and updated as \gls{az} thinks.
\begin{itemize}
	\itemb \(N(s)\): total visit count, i.d. the number of times the node \(s\) was visited. Initialized to 0. When backup is done, we increment it by 1. It is the only variable not depending on the possible legal moves.
	\itemb \(N(s, a_i)\): total visit count for each legal move \(a_i\), i.d. the number of times each legal move has been tested (visited). Initialized to 0. When backup is done, we increment it by 1.
	\itemb \(W(s, a_i)\): total action value for each legal move \(a_i\), i.d. the sum of all the value predictions of the neural network. Initialized to 0. When backup, we add the value prediction \(v\). Since \(v \in [-1, 1]\), and since the selection is always a maximum, if it is black to play, we need to add \(-1 \times v\).
	\itemb \(Q(s, a_i)\): mean action value for each legal move \(a_i\), i.d. the mean of \(W(s, a_i)\). Initialized to 0. When backup, \(Q(s, a_i) = W(s, a_i) / N(s, a_i)\).
	\itemb \(P(s, a_i)\): prior probability, the policy prediction of the neural network, i.d. the probability of selecting \(a_i\) in \(s\). We keep only the prior of legal moves. There is no backup and it is directly initialized.
\end{itemize}

\subsection{Artificial neural network}
The \gls{mcts} used by \gls{az} is guided by a neural network. I followed the same architecture as the original \gls{az} network, but with fewer blocks and fewer filters. It is a \gls{resnet} with two heads. The first head outputs a vector of probability for each move, noted \(\bm{p}\), the higher the probability is, the better the move should be. The second head outputs a scalar between -1 and 1, noted \(v\), and predicts the game outcome: -1 if black wins, 0 if it is a draw and 1 if white wins. The neural network is noted \( f_\theta = (\bm{p}, v)\), with \(\theta\) its parameters.

\section{\gls{az} algorithm}
Here we give a brief description of the \gls{az} algorithm. \gls{az} is a novel kind of reinforcement learning: it combines a neural network and a \gls{mcts} which can be seen as a "powerful policy improvement operator". A game is in progress, and it becomes \gls{az}'s turn. What is happening? First, \gls{az} evaluates the current game state. To do so, it creates a new node in a tree, the root node, following the initializations described in \ref{subsection:variables}. If the node already exists, it will skip this part.\\

Next, the engine adds a Dirichlet noise to the prior probability as follow: \(P(s, a_i) = (1 - \epsilon) p_a + \epsilon \ \eta_a\) with \(\epsilon\) a parameter balancing the amount of noise we want to add and \(\eta_a \sim Dir(\alpha)\) with \(\alpha\) the parameter of the Dirichlet distribution. This part is very important because the noise will give the ability to \gls{az} to explore new moves and not always play the same games.\\

After that, the \gls{az} engine will repeat the remaining following operations as much as it can. Usually, we can either limit the engine with the number of visits it makes, or with the time it takes to think. So \gls{az} will simulate a game starting from the root position. While the game simulated is not finished it will select the best move according to the following formula.

\[a_t = \text{argmax}_{i \in \text{legal moves}} (Q(s_t, a_i) + C(s_t) P(s_t, a_i) \frac{\sqrt{N(s_t)}}{1 + N(s_t, a_i)})\]

\(C(s)\) is an exploration rate theoretically variable but practically constant, the first term corresponds to the exploitation term (we want te see moves giving us good results) and the second term noted \(U(s_t, a_i)\), is the exploration term (we want to see moves we did not see much).\\

During the simulation, as soon as a state is not present in the tree, the engine stops and creates the corresponding node. The new created node is then evaluated by the neural network and the backup is done following what has been explained in \ref{subsection:variables} for all states encountered during the simulation. \gls{az} will then repeat this process as long as possible.

\section{Training}
To train \gls{az}, there are 2 programs that need to run concurrently to form a closed self-play training loop: selfplay and neural network training. Selfplay is made with C++. The process is multi-threaded, each thread generating selfplay games with the latest neural network. A game is played until the end, \gls{az} playing for both players. At the end, the 3 following tensors are created.
\begin{itemize}
	\itemb \(\bm{x}\): the input tensors corresponding to each game state of the game.
	\itemb \(\bm{\pi}\): the prior probabilities computed by the \gls{mcts}, equal to \( N(s, a_i) / N(s)\) for each legal move.
	\itemb \(z\): the winner of the game, either -1 if it is black or 0 if it is a draw or 1 if it is white.
\end{itemize}
Each thread writes these tensors in 3 different files concurrently, one for the input tensors, one for the prior probabilities and one for the value. When one thread writes new data, files are locked for others. The files are similar to a \gls{fifo} method. The size is controlled by a parameter called \verb|buffer_size|. When the files are full, first elements are removed.\\

Neural network training is made with Python. At the beginning of each epoch the latest data generated by the selfplay is loaded and at the end of each epoch the new trained neural network is saved. The loss used for training is: \(l = (z-v)^2 - \bm{\pi}^T \log \bm{p} + c || \theta || ^2\) with the last term corresponding to an L2 regularization.

\section{Conclusion}
\gls{az} engine is slightly better than \gls{agz}. This difference of level might be explained by the technical differences between both engines. \gls{az} does not use any symetries and continually updates the neural network without checking if it is better than the previous one. Perspective is always from current player and the neural network value predicts the excepted outcome and not the winning probability.\\

To my mind what is fabulous is the evolution of the different machines created by DeepMind. First, \gls{ag} was trained on human data. Then, by freeing the machine from the yoke of human knowledge, learning "tabula rasa", it becomes even better (\gls{agz}). Finally, \gls{az} becomes better than its predecessors generalizing the method further. But there is a sequel: MuZero \cite{muzero}. If in \gls{az} there were still the rules of the game, MuZero manages to reach similar performances without knowing them...\\

I find \gls{az} stupendous for 2 main reasons. First, it is a breakthrough for the game of Go, and being passionate about it, I can see how strong it is and how it has affected the field. Second, \gls{az} is the proof that state of the art \gls{ai} tools like reinforcement learning can lead the machine to discover things humans would had never discovered in a reasonable time. I strongly believe that \gls{ai} is more than a complicated tool for complicated tasks, and a way for humans to reach the climax of understanding, as \gls{az} has shown in the game of Go and in chess.

\newpage

\section*{Acknowledgments}
I want to sincerely thank \href{https://www.lamsade.dauphine.fr/~cazenave/}{Tristan \textsc{Cazenave}} for his teaching and for the opportunity he gave me to be part of the \gls{iasd} master. 4 years ago, far away from \gls{ai}, when I asked him if I would ever understand \glsfirst{az}, he replied I would. Hopefully this project is the confirmation of his answer.

\section*{Project structure}
\begin{itemize}[noitemsep, topsep=0pt]
	\itemb \verb|cpp| folder: program's core coded in C++.
	\itemb \verb|materials| folder: report, original papers and DeepMind \gls{az} official pseudo-code.
	\itemb \verb|networks| folder: contains neural networks used by \gls{az}.
	\itemb \verb|python| folder: script to create and train a neural network.
	\itemb \verb|selfplay| folder: selfplay data generated by \gls{az} for training.
	\itemb \verb|config.cfg| editable file with all the necessary and possible configuration.
\end{itemize}

\printglossaries
\printbibliography

\end{document}
