#include "alpha_zero/az_engine.h"
#include "alpha_zero/config.h"
#include "alpha_zero/engines.h"
#include "alpha_zero/game.h"
#include "alpha_zero/uct_engine.h"
#include "alpha_zero/zobrist.h"

Engine *create_engine(int engine_color, int index_engine) {
  if (index_engine == 0) {
    // Random engine
    Random *engine = new Random(engine_color, "Random engine");
    return engine;
  }

  else if (index_engine == 1) {
    // UCT engine
    UCT_Engine *engine = new UCT_Engine(engine_color, "UCT engine");
    return engine;
  }

  else {
    // AlphaZero engine
    AZ_Engine *engine = new AZ_Engine(engine_color, "AlphaZero engine");
    return engine;
  }
}

int main(int argc, char *argv[]) {
  // Parse arguments for config file
  std::string config_path = "../../config.cfg";
  if (argc > 1) {
    config_path = argv[argc - 1];
  }

  // Init config and zobrist hash
  init_config(config_path);
  init_zobrist_hashes(std::stoi(CONFIG["board_size"]));

  // Different seed at each run
  srand(time(NULL));

  Game game;
  bool verbose = true;

  // Note: we could use the same engine for both player, but since they are
  // considered different player They should not share the same knowledge, so we
  // create different ones for black and white

  Engine &engine1 =
      *create_engine(-1, std::stoi(CONFIG["black_engine_indice"]));
  Engine &engine2 = *create_engine(1, std::stoi(CONFIG["white_engine_indice"]));

  while (game.winner == 0) {
    engine1.play(game, verbose);
    if (game.winner == 0) {
      engine2.play(game, verbose);
    }
  }

  if (verbose == true) {
    int winner = game.winner;
    game.display();
    if (winner == -1) {
      std::cout << "Black wins.\n";
    } else if (winner == 1) {
      std::cout << "White wins.\n";
    }
  }

  return 0;
}
