#include "alpha_zero/az_engine.h"
#include "alpha_zero/config.h"
#include "alpha_zero/engines.h"
#include "alpha_zero/game.h"
#include "alpha_zero/utils.h"
#include "alpha_zero/zobrist.h"

#include <mutex>
#include <thread>

std::mutex mutex_lock;

void selfplay(int thread_index) {
  // Different seed at each call
  srand(time(NULL) + thread_index * 4);

  int board_size = std::stoi(CONFIG["board_size"]);
  int buffer_size = std::stoi(CONFIG["buffer_size"]);

  std::vector<torch::Tensor> x_vector;
  std::vector<torch::Tensor> p_vector;
  std::vector<torch::Tensor> v_vector;

  while (true) {
    Game game;

    // A single engine for both player (engine color doest not matter)
    AZ_Engine engine(42, "AlphaZero engine");

    while (game.winner == 0) {
      // X TENSOR
      x_vector.push_back(game.state);

      // Get hash and legal moves
      uint64_t hash = game.hash;
      std::vector<std::vector<int>> legal_moves = game.get_legal_moves();

      // AlphaZero thinking and playing
      engine.play(game, false);

      // Computing the policy of the network
      float state_visits = (float)engine.map_states_total_visits.at(hash);
      std::vector<int> moves_visits = engine.map_moves_total_visits.at(hash);
      torch::Tensor current_p =
          torch::zeros({board_size * board_size}, torch::device(DEVICE));
      for (size_t m = 0; m < moves_visits.size(); ++m) {
        int index = board_size * legal_moves[m][1] + legal_moves[m][2];
        current_p[index] = (float)moves_visits.at(m) / state_visits;
      }
      // P TENSOR
      p_vector.push_back(current_p);
    }

    // V TENSOR
    int value_value = game.winner;
    if (game.winner == 2) {
      value_value = 0;
    } // If it is a draw
    torch::Tensor current_v =
        torch::full({game.moves_nb, 1}, value_value, torch::device(DEVICE));
    v_vector.push_back(current_v);

    // Lock files and save tensors
    mutex_lock.lock();
    std::string selfplay_dir = CONFIG["selfplay_dir"];
    std::string selfplay_dir_x = selfplay_dir + "/x.pt";
    std::string selfplay_dir_p = selfplay_dir + "/p.pt";
    std::string selfplay_dir_v = selfplay_dir + "/v.pt";

    save_tensor_to_file(torch::stack(x_vector, 0), selfplay_dir_x, buffer_size);
    save_tensor_to_file(torch::stack(p_vector, 0), selfplay_dir_p, buffer_size);
    save_tensor_to_file(torch::cat(v_vector, 0), selfplay_dir_v, buffer_size);
    mutex_lock.unlock();

    // Empty vectors of current tensors
    x_vector.clear();
    p_vector.clear();
    v_vector.clear();
  }
}

int main(int argc, char *argv[]) {
  // Parse arguments for config file
  std::string config_path = "../../config.cfg";
  if (argc > 1) {
    config_path = argv[argc - 1];
  }

  // Init config and zobrist hash
  init_config(config_path);
  init_zobrist_hashes(std::stoi(CONFIG["board_size"]));

  // Launch threads
  std::vector<std::thread> jobs;
  for (int th_index = 0; th_index < std::stoi(CONFIG["nb_jobs"]); ++th_index) {
    jobs.emplace_back(selfplay, th_index);
  }

  for (auto &th : jobs) {
    th.join();
  }

  return 0;
}
