#include <vector>

#include "alpha_zero/engines.h"

Engine::Engine(int color, std::string name) {
  this->name = name;
  this->color = color;
}

void Random::play(Game &game, const bool &verbose) {
  if (verbose == true) {
    game.display();
  }

  std::vector<std::vector<int>> legal_moves = game.get_legal_moves();
  int random_index = rand() % (legal_moves.size() - 1);
  std::vector<int> move = legal_moves[random_index];

  if (verbose == true) {
    std::cout << "Engine: " << name << '\n';
    std::cout << "Number of legal moves: " << legal_moves.size() << '\n';
    std::cout << "Move to play: " << '(' << move[1] << ", " << move[2] << ")\n";
    getchar();
  }

  game.play_move(move);
}
