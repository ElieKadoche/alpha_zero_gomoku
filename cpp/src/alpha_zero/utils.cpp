#include <filesystem>

#include "alpha_zero/config.h"
#include "alpha_zero/utils.h"

void save_tensor_to_file(const torch::Tensor &tensor, const std::string path,
                         const int &buffer_size) {
  torch::Tensor new_tensor = tensor.to(DEVICE);

  if (std::filesystem::exists(path)) {
    // Previous tensor
    torch::Tensor previous_tensor;
    torch::load(previous_tensor, path, DEVICE);

    // Compute new tensor
    new_tensor = torch::cat({previous_tensor, tensor}, 0).to(DEVICE);
  }

  // Check if size is smaller than buffer size
  int buffer_current_size = new_tensor.sizes()[0];
  if (buffer_current_size > buffer_size) {
    // Removed first elements
    new_tensor =
        new_tensor.slice(0, buffer_current_size - buffer_size).to(DEVICE);
  }

  // Save new tensor
  torch::save(new_tensor, path);
}

void update_tensor(Game &game, const std::vector<int> &last_move) {
  // Player plane
  torch::Tensor player_plane;
  if (game.turn == -1) {
    player_plane = torch::ones({1, game.board_size, game.board_size},
                               torch::device(DEVICE));
  } else {
    player_plane = torch::zeros({1, game.board_size, game.board_size},
                                torch::device(DEVICE));
  }

  // Current state planes
  // OTHER OPTION: newest planes at the end
  // torch::Tensor player_current_plane = game.state.select(0,
  // game.history_planes * 2 - 1).clone().to(DEVICE); torch::Tensor
  // opponent_current_plane = game.state.select(0, game.history_planes * 2 -
  // 2).clone().to(DEVICE);
  torch::Tensor player_current_plane =
      game.state.select(0, 1).clone().to(DEVICE);
  torch::Tensor opponent_current_plane =
      game.state.select(0, 0).clone().to(DEVICE);
  opponent_current_plane[last_move.at(1)][last_move.at(2)] = 1.0;
  player_current_plane = torch::unsqueeze(player_current_plane, 0);
  opponent_current_plane = torch::unsqueeze(opponent_current_plane, 0);

  // Previous planes
  // OTHER OPTION: newest planes at the end
  // torch::Tensor state = game.state.slice(0, 2, game.history_planes * 2,
  // 1).clone().to(DEVICE); // We remove the first 2 and the last
  torch::Tensor state = game.state.slice(0, 0, game.history_planes * 2 - 2, 1)
                            .clone()
                            .to(DEVICE); // We remove the last 3 planes

  // We invert previous planes because perspective is from current player
  torch::Tensor s1 = torch::unsqueeze(state.select(0, 1), 0);
  torch::Tensor s2 = torch::unsqueeze(state.select(0, 0), 0);
  torch::Tensor previous_planes = torch::cat({s1, s2}, 0);
  for (int64_t i = 2; i < state.sizes()[0]; i += 2) {
    s1 = torch::unsqueeze(state.select(0, i + 1).clone(), 0);
    s2 = torch::unsqueeze(state.select(0, i).clone(), 0);
    previous_planes = torch::cat({previous_planes, s1, s2}, 0);
  }

  // We concatenate everything
  // OTHER OPTION: newest planes at the end
  // game.state = torch::cat({previous_planes, player_current_plane,
  // opponent_current_plane, player_plane}, 0);
  game.state = torch::cat({player_current_plane, opponent_current_plane,
                           previous_planes, player_plane},
                          0);
}

void predict(torch::jit::script::Module &network, const torch::Tensor &state,
             std::vector<float> &policy, float &value) {
  // State is shape [X, B, B], predict takes shape [1, X, B, B]
  std::vector<torch::jit::IValue> inputs{torch::unsqueeze(state, 0)};
  auto outputs = network.forward(inputs).toTuple();
  torch::Tensor out1 = outputs->elements()[0].toTensor(); // Size [1, B * B]
  torch::Tensor out2 = outputs->elements()[1].toTensor(); // Size [1, 1]
  policy = std::vector<float>(out1.data_ptr<float>(),
                              out1.data_ptr<float>() + out1.numel());
  value = out2[0][0].item<float>();
}

std::vector<float>
get_legal_prior(torch::jit::script::Module &network, const Game &game,
                const std::vector<std::vector<int>> &legal_moves) {
  float value;
  std::vector<float> policy;
  predict(network, game.state, policy, value);
  std::vector<float> legal_prior;
  for (size_t i = 0; i < legal_moves.size(); ++i) {
    float prior =
        policy[game.board_size * legal_moves[i][1] + legal_moves[i][2]];
    legal_prior.push_back(prior);
  }
  return legal_prior;
}

float evaluate(torch::jit::script::Module &network,
               const torch::Tensor &state) {
  float value;
  std::vector<float> policy;
  predict(network, state, policy, value);
  return value;
}
