#include <chrono>
#include <filesystem>
#include <math.h>
#include <random>
#include <stdexcept>
#include <thread>

#include "alpha_zero/az_engine.h"
#include "alpha_zero/config.h"
#include "alpha_zero/utils.h"

AZ_Engine::AZ_Engine(int color, std::string name) : Engine(color, name) {
  c = std::stof(CONFIG["az_exploration_constant"]);
  max = std::stoi(CONFIG["max_visits"]);
  root_exploration_fraction = std::stof(CONFIG["root_exploration_fraction"]);
  root_dirichlet_alpha = std::stof(CONFIG["root_dirichlet_alpha"]);

  try {
    // If python saves the neural network (after a training), we wait
    std::string lock_path = CONFIG["net_path"] + ".lock";
    while (std::filesystem::exists(lock_path)) {
      std::this_thread::sleep_for(std::chrono::seconds(3));
    }

    // Load the neural network
    this->network = torch::jit::load(CONFIG["net_path"]);
    network.to(DEVICE);
  } catch (const c10::Error &e) {
    throw std::invalid_argument("Error loading the model");
  }
}

// DIRICHLET NOISE
void AZ_Engine::add_dirichlet_noise(const uint64_t &hash) {
  std::random_device rd;
  std::mt19937 generator(rd());
  std::gamma_distribution<double> distribution(root_dirichlet_alpha, 1);

  for (size_t i = 0; i < map_moves_prior_proba.at(hash).size(); ++i) {
    float noise = (float)distribution(generator);
    float raw_p = map_moves_prior_proba.at(hash).at(i);
    map_moves_prior_proba.at(hash).at(i) =
        (1 - root_exploration_fraction) * raw_p +
        root_exploration_fraction * noise;
  }
}

// SELECTION
int AZ_Engine::select_move(const Game &game) const {
  std::vector<float> putcs;
  std::vector<std::vector<int>> legal_moves = game.get_legal_moves();

  for (size_t m = 0; m < legal_moves.size(); ++m) {
    // Q(s,a), exploitation term
    float q = map_moves_mean_action_value.at(game.hash).at(m);

    // U(s,a), exploration term
    float p = map_moves_prior_proba.at(game.hash).at(m);
    float ns = map_states_total_visits.at(game.hash);       // N(s)
    float nsa = map_moves_total_visits.at(game.hash).at(m); // N(s,a)
    float u = c * p * (sqrt(ns) / (1 + nsa));

    // Compute final formula
    putcs.push_back(q + u);
  }

  int best_move_index = 0;
  auto max = std::max_element(putcs.begin(), putcs.end());
  best_move_index = std::distance(putcs.begin(), max);
  return best_move_index;
}

// BACKUP
void AZ_Engine::backup(const int &last_turn,
                       const std::vector<uint64_t> &memory_states,
                       const std::vector<int> &memory_actions, const float &v) {
  // Since v is in [-1;1], -1 if black wins, 0 if draw and 1 if white wins and
  // since in selection we always take the maximum We need to invert v when it
  // is black to play
  float last_player = last_turn * -1;

  for (size_t t = memory_states.size(); t-- > 0;) {
    map_states_total_visits.at(memory_states[t]) += 1; // N(s) += 1
    map_moves_total_visits.at(memory_states[t]).at(memory_actions[t]) +=
        1; // N(s,a) += 1
    map_moves_total_action_value.at(memory_states[t]).at(memory_actions[t]) +=
        last_player * v; // W(s,a) +=/-= v

    float w = map_moves_total_action_value.at(memory_states[t])
                  .at(memory_actions[t]); // W(s,a)
    float n = map_moves_total_visits.at(memory_states[t])
                  .at(memory_actions[t]); // N(s,a)
    map_moves_mean_action_value.at(memory_states[t]).at(memory_actions[t]) =
        w / n;         // Q(s,a) = W / N
    last_player *= -1; // As we backup, last player changes
  }
}

// NEW_NODE
void AZ_Engine::new_node(const Game &game) {
  map_states_total_visits[game.hash] = 1; // N(s)
  std::vector<std::vector<int>> legal_moves = game.get_legal_moves();
  map_moves_total_visits[game.hash] =
      std::vector<int>(legal_moves.size(), 0); // N(s,a)
  map_moves_total_action_value[game.hash] =
      std::vector<float>(legal_moves.size(), 0); // W(s,a)
  map_moves_mean_action_value[game.hash] =
      std::vector<float>(legal_moves.size(), 0); // Q(s,a)
  map_moves_prior_proba[game.hash] =
      get_legal_prior(network, game, legal_moves); // P(s,a)
}

// SIM_TREE
void AZ_Engine::sim_tree(Game &game, std::vector<uint64_t> &memory_states,
                         std::vector<int> &memory_actions) {
  while (game.winner == 0) {
    if (map_states_total_visits.find(game.hash) ==
        map_states_total_visits.end()) {
      new_node(game);
      return;
    }
    int move_index = select_move(game);
    memory_states.push_back(game.hash);
    memory_actions.push_back(move_index);
    std::vector<std::vector<int>> legal_moves = game.get_legal_moves();
    game.play_move(legal_moves[move_index]);
  }
  return;
}

// MCTS SEARCH
void AZ_Engine::run_mcts(const Game &game) {
  // If the node does not exist, create it
  if (map_states_total_visits.find(game.hash) ==
      map_states_total_visits.end()) {
    new_node(game);
  }
  add_dirichlet_noise(game.hash); // Add dirichlet noise

  // While we can (simulations)
  for (int simulation = 0; simulation < max; ++simulation) {
    Game game_bis = game;
    std::vector<int> memory_actions;
    std::vector<uint64_t> memory_states;
    sim_tree(game_bis, memory_states, memory_actions);
    float v = evaluate(network, game_bis.state);
    backup(game_bis.turn, memory_states, memory_actions, v);
  }
}

void AZ_Engine::play(Game &game, const bool &verbose) {
  if (verbose == true) {
    game.display();
  }

  run_mcts(game);

  // Choose the move with the highest visits
  std::vector<int> total_visits = map_moves_total_visits[game.hash];
  auto max = std::max_element(total_visits.begin(), total_visits.end());
  int best_move_index = std::distance(total_visits.begin(), max);
  std::vector<std::vector<int>> legal_moves = game.get_legal_moves();
  std::vector<int> move = legal_moves[best_move_index];

  if (verbose == true) {
    std::cout << "Engine: " << name << '\n';
    std::cout << "Visits for each legal move: ";
    for (size_t m = 0; m < legal_moves.size(); ++m) {
      std::cout << '(' << legal_moves[m][1] << "," << legal_moves[m][2]
                << "): " << total_visits[m] << " - ";
    }

    float v = evaluate(network, game.state);
    std::cout << "\nMove to play: " << '(' << move[1] << ", " << move[2]
              << ") --- Visits: " << total_visits[best_move_index]
              << " --- Value: " << v << '\n';
    getchar();
  }

  game.play_move(move);
}
