#include <math.h>
// #include <chrono> // If using seconds for max

#include "alpha_zero/config.h"
#include "alpha_zero/uct_engine.h"
#include "alpha_zero/zobrist.h"

UCT_Engine::UCT_Engine(int color, std::string name) : Engine(color, name) {
  c = std::stof(CONFIG["uct_exploration_constant"]);
  max = std::stoi(CONFIG["max_playouts"]);
}

// PLAYOUT
int UCT_Engine::sim_default(const Game &game) const {
  Game game_bis = game;
  while (game_bis.winner == 0) {
    std::vector<std::vector<int>> legal_moves = game_bis.get_legal_moves();
    int random_index = rand() % (legal_moves.size() - 1);
    game_bis.play_move(legal_moves.at(random_index));
  }
  return game_bis.winner;
}

// SELECTION
int UCT_Engine::select_move(const Game &game) const {
  std::vector<float> ucbs;
  std::vector<std::vector<int>> legal_moves = game.get_legal_moves();

  float exploration_sign =
      -1.0; // Because we will take the min if opponent to play
  if (game.turn == color) {
    exploration_sign = 1.0;
  }

  for (size_t m = 0; m < legal_moves.size(); ++m) {
    float n = (float)map_states_total_playouts.at(game.hash);
    float ni = (float)map_moves_total_playouts.at(game.hash).at(m);
    float qi = map_moves_q_values.at(game.hash).at(m);

    if (ni == 0) {
      return m;
    } // No playouts --> we need to explore the move before the others
    ucbs.push_back(qi + exploration_sign * (c * sqrt(log(n) / ni)));
  }

  int best_move_index = 0;
  if (game.turn == color) {
    // If engine to play, we want max(ucbs)
    auto max = std::max_element(ucbs.begin(), ucbs.end());
    best_move_index = std::distance(ucbs.begin(), max);
  } else {
    // If opponent to play, we want min(ucbs)
    auto min = std::min_element(ucbs.begin(), ucbs.end());
    best_move_index = std::distance(ucbs.begin(), min);
  }

  return best_move_index;
}

// BACKUP
void UCT_Engine::backup(const std::vector<uint64_t> &memory_states,
                        const std::vector<int> &memory_actions,
                        const int &winner) {
  for (size_t t = 0; t < memory_states.size(); ++t) {
    map_states_total_playouts.at(memory_states[t]) += 1;
    map_moves_total_playouts.at(memory_states[t]).at(memory_actions[t]) += 1;

    // Update Q(s,a)
    float z = 0;
    if (color == winner) {
      z = 1;
    }
    float ni = (float)map_moves_total_playouts.at(memory_states[t])
                   .at(memory_actions[t]);
    float old_q = map_moves_q_values.at(memory_states[t]).at(memory_actions[t]);
    map_moves_q_values.at(memory_states[t]).at(memory_actions[t]) +=
        (z - old_q) / ni;
  }
}

// NEW_NODE
void UCT_Engine::new_node(const Game &game) {
  map_states_total_playouts[game.hash] =
      1; // After the creation of a node, a playout is systematically done
  std::vector<std::vector<int>> legal_moves = game.get_legal_moves();
  map_moves_total_playouts[game.hash] = std::vector<int>(legal_moves.size(), 0);
  map_moves_q_values[game.hash] = std::vector<float>(legal_moves.size(), 0);
}

// SIM_TREE
void UCT_Engine::sim_tree(Game &game, std::vector<uint64_t> &memory_states,
                          std::vector<int> &memory_actions) {
  while (game.winner == 0) {
    if (map_states_total_playouts.find(game.hash) ==
        map_states_total_playouts.end()) {
      new_node(game);
      return;
    }
    int move_index = select_move(game);
    memory_states.push_back(game.hash);
    memory_actions.push_back(move_index);
    std::vector<std::vector<int>> legal_moves = game.get_legal_moves();
    game.play_move(legal_moves[move_index]);
  }
  return;
}

// MCTS SEARCH
void UCT_Engine::simulate(const Game &game) {
  // While we can (simulations or time)
  // auto t1 = std::chrono::system_clock::now();
  // while
  // (std::chrono::duration_cast<std::chrono::seconds>(std::chrono::system_clock::now()
  // - t1).count() < max) {
  for (int simulation = 0; simulation < max; ++simulation) {
    Game game_bis = game;
    std::vector<int> memory_actions;
    std::vector<uint64_t> memory_states;
    sim_tree(game_bis, memory_states, memory_actions);
    int z = sim_default(game_bis);
    backup(memory_states, memory_actions, z);
  }
}

void UCT_Engine::play(Game &game, const bool &verbose) {
  if (verbose == true) {
    game.display();
  }

  simulate(game);

  // Choose the move with the highest playouts
  std::vector<int> total_playouts = map_moves_total_playouts[game.hash];
  auto max = std::max_element(total_playouts.begin(), total_playouts.end());
  int best_move_index = std::distance(total_playouts.begin(), max);
  std::vector<std::vector<int>> legal_moves = game.get_legal_moves();
  std::vector<int> move = legal_moves[best_move_index];

  if (verbose == true) {
    std::cout << "Engine: " << name << '\n';
    std::cout << "Playouts for each legal move: ";
    for (size_t m = 0; m < legal_moves.size(); ++m) {
      std::cout << '(' << legal_moves[m][1] << "," << legal_moves[m][2]
                << "): " << total_playouts[m] << " - ";
    }

    std::cout << "\nMove to play: " << '(' << move[1] << ", " << move[2]
              << ") --- Playouts: " << total_playouts[best_move_index] << '\n';
    getchar();
  }

  game.play_move(move);
}
