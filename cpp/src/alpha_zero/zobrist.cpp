#include "alpha_zero/zobrist.h"

std::map<std::vector<int>, uint64_t> ZOBRIST;

void init_zobrist_hashes(const int &board_size) {
  for (int row = 0; row < board_size; ++row) {
    for (int col = 0; col < board_size; ++col) {
      uint64_t black_hash = rand() % UINT64_MAX;
      uint64_t white_hash = rand() % UINT64_MAX;

      ZOBRIST[std::vector<int>{-1, row, col}] = black_hash;
      ZOBRIST[std::vector<int>{1, row, col}] = white_hash;
    }
  }
}

uint64_t get_next_hash(const uint64_t &previous_hash,
                       const std::vector<int> &move) {
  return previous_hash ^ ZOBRIST.at(move);
}
