#include "alpha_zero/config.h"

#include <algorithm>
#include <fstream>
#include <regex>

torch::DeviceType DEVICE;
std::map<std::string, std::string> CONFIG;

void init_config(const std::string &config_file) {
  std::regex comment_regex("# (.*)");
  std::ifstream infile(config_file);
  for (std::string line; std::getline(infile, line);) {
    if (!std::regex_match(line, comment_regex) && line.length() > 0) {
      std::string key = line.substr(0, line.find(" = "));
      std::string value = line.substr(line.find(" = ") + 3, line.length());
      CONFIG[key] = value;
    }
  }

  DEVICE = torch::kCPU;
  if (std::stoi(CONFIG["use_cuda"])) {
    DEVICE = torch::kCUDA;
  }
}
