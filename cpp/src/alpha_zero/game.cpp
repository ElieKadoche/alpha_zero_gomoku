#include <algorithm>
#include <iostream>

#include "alpha_zero/config.h"
#include "alpha_zero/game.h"
#include "alpha_zero/utils.h"
#include "alpha_zero/zobrist.h"

Game::Game() {
  board_size = std::stoi(CONFIG["board_size"]);
  nb_aligned = std::stoi(CONFIG["nb_aligned"]);
  history_planes = std::stoi(CONFIG["history_planes"]);

  turn = -1; // Black always begins
  hash = 0;  // Hash for empty board
  winner = 0;
  moves_nb = 0;

  // Tensor
  state = torch::zeros({2 * history_planes, board_size, board_size},
                       torch::device(DEVICE));
  torch::Tensor player_plane =
      torch::ones({1, board_size, board_size}, torch::device(DEVICE));
  state = torch::cat({state, player_plane},
                     0); // On same device of state and player_plane

  board = new int *[board_size];
  for (int row = 0; row < board_size; ++row) {
    board[row] = new int[board_size];
    for (int col = 0; col < board_size; ++col) {
      board[row][col] = 0; // Empty board
    }
  }
}

Game::Game(const Game &g) {
  board_size = g.board_size;
  nb_aligned = g.nb_aligned;
  history_planes = g.history_planes;

  turn = g.turn;
  hash = g.hash;
  winner = g.winner;
  moves_nb = g.moves_nb;
  state = g.state.clone().to(DEVICE);

  board = new int *[board_size];
  for (int row = 0; row < board_size; ++row) {
    board[row] = new int[board_size];
    for (int col = 0; col < board_size; ++col) {
      board[row][col] = g.board[row][col];
    }
  }
}

Game::~Game() {
  for (int row = 0; row < board_size; ++row) {
    delete[] board[row];
  }
  delete[] board;
}

Game &Game::operator=(const Game &g) {
  board_size = g.board_size;
  nb_aligned = g.nb_aligned;
  history_planes = g.history_planes;

  turn = g.turn;
  hash = g.hash;
  winner = g.winner;
  moves_nb = g.moves_nb;
  state = g.state.clone().to(DEVICE);

  board = new int *[board_size];
  for (int row = 0; row < board_size; ++row) {
    board[row] = new int[board_size];
    for (int col = 0; col < board_size; ++col) {
      board[row][col] = g.board[row][col];
    }
  }
  return *this;
}

bool Game::is_move_valid(const std::vector<int> &move) const {
  // Not valid if not player's turn
  if (move[0] != turn) {
    return false;
  }

  // Not valid if outside the board
  if (move[1] < 0 || move[1] >= board_size) {
    return false;
  }

  if (move[2] < 0 || move[2] >= board_size) {
    return false;
  }

  // Not valid if there is already a stone
  if (board[move[1]][move[2]] != 0) {
    return false;
  }

  return true;
}

std::vector<std::vector<int>> Game::get_legal_moves() const {
  std::vector<std::vector<int>> legal_moves;
  for (int row = 0; row < board_size; ++row) {
    for (int col = 0; col < board_size; ++col) {
      std::vector<int> move{turn, row, col};
      if (is_move_valid(move)) {
        legal_moves.push_back(move);
      }
    }
  }
  return legal_moves;
}

void Game::play_move(const std::vector<int> &move) {
  // Play the move
  board[move[1]][move[2]] = move[0];

  // Update winner
  winner = update_winner(move);

  // Update hash
  hash = get_next_hash(hash, move);

  // Change turn
  moves_nb += 1;
  turn = move[0] * -1;

  // Update tensor
  update_tensor(*this, move);
}

int Game::update_winner(const std::vector<int> &last_move) const {
  // Horizontal check
  int CA = std::max(0, last_move[2] - nb_aligned + 1); // Column start
  int CB = std::min(last_move[2] + nb_aligned - 1, board_size - 1);
  int current_aligned = 0;
  for (int col = CA; col <= CB; ++col) {
    if (board[last_move[1]][col] == last_move[0]) {
      ++current_aligned;
    } else {
      current_aligned = 0;
    }
    if (current_aligned == nb_aligned) {
      return last_move[0];
    }
  }

  // Vertical check
  int RA = std::max(0, last_move[1] - nb_aligned + 1);              // Row start
  int RB = std::min(last_move[1] + nb_aligned - 1, board_size - 1); // Row end
  current_aligned = 0;
  for (int row = RA; row <= RB; ++row) {
    if (board[row][last_move[2]] == last_move[0]) {
      ++current_aligned;
    } else {
      current_aligned = 0;
    }
    if (current_aligned == nb_aligned) {
      return last_move[0];
    }
  }

  // Steps for diagonals
  int DUL =
      std::min(last_move[1] - RA, last_move[2] - CA); // Diagonal upper left
  int DBR =
      std::min(RB - last_move[1], CB - last_move[2]); // Diagonal bottom right
  int DBL =
      std::min(RB - last_move[1], last_move[2] - CA); // Diagonal bottom left
  int DUR =
      std::min(last_move[1] - RA, CB - last_move[2]); // Diagonal upper right

  // Diagonal check (up to bottom)
  current_aligned = 0;
  for (int s = 0; s <= DUL + DBR; ++s) {
    if (board[last_move[1] - DUL + s][last_move[2] - DUL + s] == last_move[0]) {
      ++current_aligned;
    } else {
      current_aligned = 0;
    }
    if (current_aligned == nb_aligned) {
      return last_move[0];
    }
  }

  // Diagonal check (bottom to up)
  current_aligned = 0;
  for (int s = 0; s <= DBL + DUR; ++s) {
    if (board[last_move[1] + DBL - s][last_move[2] - DBL + s] == last_move[0]) {
      ++current_aligned;
    } else {
      current_aligned = 0;
    }
    if (current_aligned == nb_aligned) {
      return last_move[0];
    }
  }

  // This case should be very rare (no more move to play)
  if (moves_nb > board_size * board_size) {
    return 2;
  }

  return 0;
}

void Game::display() const {
  std::cout << "----------------------------------\n";
  std::cout << "Moves number: " << moves_nb << '\n';
  std::cout << "Board hash: " << hash << '\n';
  if (turn == -1) {
    std::cout << "Black (X) to play\n\n";
  } else {
    std::cout << "White (O) to play\n\n";
  }

  std::string cols = "   a b c d e f g h j k l m n o p q r s t u v w x y z";
  cols = cols.substr(0, 2 + 2 * board_size);

  std::cout << cols << '\n';
  for (int row = 0; row < board_size; ++row) {
    if (row < 10) {
      std::cout << ' ' << row;
    } else {
      std::cout << row;
    }
    for (int col = 0; col < board_size; ++col) {
      if (board[row][col] == -1) { // Black (cross)
        // std::cout << " X";
        std::cout << "\033[1m\033[36m"
                  << " X"
                  << "\033[0m"; // ANSI
      } else if (board[row][col] == 1) {
        // std::cout << " O"; // White (circle)
        std::cout << "\033[1m\033[35m"
                  << " O"
                  << "\033[0m"; // ANSI
      } else {
        std::cout << " .";
      }
    }
    if (row < 10) {
      std::cout << ' ' << row << '\n';
    } else {
      std::cout << ' ' << row << '\n';
    }
  }
  std::cout << cols << '\n';
}
