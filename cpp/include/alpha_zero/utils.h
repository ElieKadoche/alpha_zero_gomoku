#ifndef ALPHA_ZERO_UTILS
#define ALPHA_ZERO_UTILS

#include <iostream>
#include <torch/script.h>
#include <torch/torch.h>
#include <vector>

#include "alpha_zero/game.h"

// Save tensor to file
void save_tensor_to_file(const torch::Tensor &tensor, const std::string path,
                         const int &buffer_size);

// Update the tensor representation of the game after a move is played
void update_tensor(Game &game, const std::vector<int> &last_move);

// Neural network prediction
void predict(torch::jit::script::Module &network, const torch::Tensor &state,
             std::vector<float> &policy, float &value);

// Get legal prior
std::vector<float>
get_legal_prior(torch::jit::script::Module &network, const Game &game,
                const std::vector<std::vector<int>> &legal_moves);

// Get value
float evaluate(torch::jit::script::Module &network, const torch::Tensor &state);

#endif // ALPHA_ZERO_UTILS
