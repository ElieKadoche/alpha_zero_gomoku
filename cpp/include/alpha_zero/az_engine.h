#ifndef ALPHA_ZERO_AZ_ENGINE
#define ALPHA_ZERO_AZ_ENGINE

#include <iostream>
#include <map>
#include <torch/script.h>
#include <torch/torch.h>
#include <vector>

#include "alpha_zero/engines.h"
#include "alpha_zero/game.h"

class AZ_Engine : public Engine {
private:
  // Or max number of simulations per turn
  int max;

  // Exploration constant
  float c;

  // Constant epsilon for the root when adding Dirichlet noise: P(s,a) = (1 -
  // eps) + eps * noise
  float root_exploration_fraction;

  // With noise = D(alpha), the dirichlet law and its following parameters alpha
  float root_dirichlet_alpha;

  // The neural network
  torch::jit::script::Module network;

  // Map for total action value per action per state W(s,a)
  std::map<uint64_t, std::vector<float>> map_moves_total_action_value;

  // Map for mean action value per action per state Q(s,a)
  std::map<uint64_t, std::vector<float>> map_moves_mean_action_value;

  // Map for prior probability per action per state P(s,a)
  std::map<uint64_t, std::vector<float>> map_moves_prior_proba;

  // ADD DIRICHLET NOISE
  void add_dirichlet_noise(const uint64_t &hash);

  // SELECTION
  int select_move(const Game &game) const;

  // BACKUP
  void backup(const int &last_turn, const std::vector<uint64_t> &memory_states,
              const std::vector<int> &memory_actions, const float &v);

  // NEW NODE
  void new_node(const Game &game);

  // SIM TREE
  void sim_tree(Game &game, std::vector<uint64_t> &memory_states,
                std::vector<int> &memory_actions);

  // MCTS SEARCH
  void run_mcts(const Game &game);

public:
  // Map for total visits done per game state N(s)
  std::map<uint64_t, int> map_states_total_visits;

  // Map for total visits done per action per state N(s,a)
  std::map<uint64_t, std::vector<int>> map_moves_total_visits;

  // Constructor
  AZ_Engine(int color, std::string name);

  // Play function
  void play(Game &game, const bool &verbose);
};

#endif // ALPHA_ZERO_AZ_ENGINE
