#ifndef ALPHA_ZERO_GAME
#define ALPHA_ZERO_GAME

#include <torch/torch.h>
#include <vector>

// A move is a vector of 3 integers (turn, row, col)

class Game {
private:
  // For Gomoku, it is simpler to udpate the winner when a move is played
  // winner = 0 if there is no winner, -1 if Black and 1 if White
  // winner = 2 if it is a draw, but this case should never happen
  int update_winner(const std::vector<int> &move) const;

public:
  int turn;
  int **board;
  int moves_nb;
  int board_size;
  int nb_aligned;
  uint64_t hash;

  // 0 if game not finished, -1 if black, 1 if white, 2 if draw
  int winner;

  // Tensor representation of the state
  torch::Tensor state;
  int history_planes;

  // Constructor
  Game();

  // Deep copy constructor
  Game(const Game &g);

  // Destructor
  ~Game();

  // Operator =
  Game &operator=(const Game &g);

  // Is move valid
  bool is_move_valid(const std::vector<int> &move) const;

  // Get legal moves
  std::vector<std::vector<int>> get_legal_moves() const;

  // Play a move
  void play_move(const std::vector<int> &move);

  // Display board
  void display() const;
};

#endif // ALPHA_ZERO_GAME
