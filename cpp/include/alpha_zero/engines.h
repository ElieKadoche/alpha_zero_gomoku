#ifndef ALPHA_ZERO_ENGINES
#define ALPHA_ZERO_ENGINES

#include <iostream>

#include "alpha_zero/game.h"

class Engine {
public:
  // Engine color, either -1 (black) or 1 (white)
  int color;

  // Engine name
  std::string name;

  // Constructor
  Engine(int color, std::string name);

  // 1 virtual method: the class is abstract
  // Child class musts implement this method
  virtual void play(Game &game, const bool &verbose) = 0;
};

class Random : public Engine {
public:
  Random(int color, std::string name) : Engine(color, name){};
  void play(Game &game, const bool &verbose);
};

#endif // ALPHA_ZERO_ENGINES
