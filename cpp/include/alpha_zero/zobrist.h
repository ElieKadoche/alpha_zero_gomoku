#ifndef ALPHA_ZERO_ZOBRIST
#define ALPHA_ZERO_ZOBRIST

#include <map>
#include <vector>

// Each move has a hash
extern std::map<std::vector<int>, uint64_t> ZOBRIST;

// Init moves hashes
void init_zobrist_hashes(const int &board_size);

// Get next hash
uint64_t get_next_hash(const uint64_t &previous_hash,
                       const std::vector<int> &move);

#endif // ALPHA_ZERO_ZOBRIST
