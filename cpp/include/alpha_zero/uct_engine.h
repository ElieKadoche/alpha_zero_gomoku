#ifndef ALPHA_ZERO_UCT_ENGINE
#define ALPHA_ZERO_UCT_ENGINE

#include <iostream>
#include <map>
#include <vector>

#include "alpha_zero/engines.h"
#include "alpha_zero/game.h"

class UCT_Engine : public Engine {
private:
  // Max number of simulations
  int max;

  // Exploration constant
  float c;

  // Map for total playouts done per state N(s)
  std::map<uint64_t, int> map_states_total_playouts;

  // Map for total playouts done per legal move per state N(s,a)
  std::map<uint64_t, std::vector<int>> map_moves_total_playouts;

  // Map for Q values per legal move per state Q(s,a) (ratio wins / played)
  std::map<uint64_t, std::vector<float>> map_moves_q_values;

  // UCT functions described in the paper of Sylvain Gelly and David Silver
  // ----------------------------------------------------------------------

  // PLAYOUT
  int sim_default(const Game &game) const;

  // SELECTION (get index of best move)
  int select_move(const Game &game) const;

  // BACKUP
  void backup(const std::vector<uint64_t> &memory_states,
              const std::vector<int> &memory_actions, const int &winner);

  // NEW_NODE
  void new_node(const Game &game);

  // SIM_TREE
  void sim_tree(Game &game, std::vector<uint64_t> &memory_states,
                std::vector<int> &memory_actions);

  // MCTS SEARCH
  void simulate(const Game &game);

public:
  // Constructor
  UCT_Engine(int color, std::string name);

  // Play function
  void play(Game &game, const bool &verbose);
};

#endif // ALPHA_ZERO_UCT_ENGINE
