#ifndef ALPHA_ZERO_CONFIG
#define ALPHA_ZERO_CONFIG

#include <iostream>
#include <map>
#include <torch/torch.h>

// Device for libtorch Tensors
extern torch::DeviceType DEVICE;

// Parameters are kept in a map of strings
extern std::map<std::string, std::string> CONFIG;

// Initialized config
void init_config(const std::string &config_file);

#endif // ALPHA_ZERO_CONFIG
