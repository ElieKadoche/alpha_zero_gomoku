# alpha_zero_gomoku

This project aims to reproduce the AlphaZero algorithm for the game of Gomoku. Goals are the following.

- Having a simple program with the bare configuration for simplicity.
- Having a well optimized program using C++ and Python for efficient and fast computations.

## Rules

Gomoku, or Five in a Row is played with Go stones on a Go board. Players alternate turns playing a stone of their color on an empty intersection. The winner is the first player to get an unbroken row of 5 stones horizontally, vertically or diagonally. Black always begins. Even if this game is already solved, it represents a good example to understand how AlphaZero works.

## Prerequisites

For efficency, the core of the program has been written in C++. Python has only been used to create and train the neural network. You need to have Python 3.7 (at least) and CMake 3.16.5 (at least) installed on your computer. It has been tested with GNU Make 3.81 and the Apple clang 11.0.3, other compilators has not been tested. You need to install libtorch 1.4 on your system, download it from [here](https://pytorch.org/). To install the program, commands are the following.

```shell
git clone https://gitlab.com/ElieKadoche/alpha_zero_gomoku.git
cd alpha_zero_gomoku/
pip install -r requirements.txt
cd cpp/
mkdir build
cd build
cmake -DCMAKE_PREFIX_PATH=<your_absolute_path_to_libtorch> ..
make
cd ../..
```

## Structure

- [`cpp`](./cpp): C++ source code. Playing and selfplay scripts. AlphaZero engine in C++.
- [`materials`](./materials): general documentation.
- [`networks`](./networks): saved neural nets.
- [`python`](./python): Python source code. Scripts to create and train a neural net. AlphaZero engine in Python in [`python/alpha_zero`](./python/alpha_zero).
- [`selfplay`](./selfplay): generated selfplay data.
- [`config.cfg`](./config.cfg): editable configuration file.

## Usage

### Configuration file

Before using the program, you can read the [configuration file](./config.cfg). Everything you need to set up is in this file.

### Playing script

You can run `./cpp/build/play --config ./confg.cfg` to play a game between 2 engines. The choice of players is made in the configuration file. Engines available are: random engine, UCT (Upper Confidence bounds applied to Trees) engine and AlphaZero engine.

### Training AlphaZero

First, you need to create a neural network. You can skip this part if you want to resume a training.

```shell
python ./python/neural_network.py --config ./config.cfg
```

Then, to train AlphaZero, there are 2 programs that need to run concurrently to form a closed self-play training loop: selfplay and neural network training. To generate selfplay games, you can use either C++ or Python. Passing from one to the other, you will need to delete all generated data in [selfplay](./selplay) directory.

```shell
./cpp/build/selfplay --config ./config.cfg & # For C++
python ./python/alpha_zero/selfplay.py --config ./config.cfg & # For Python
```

The network training is finally launched by executing this command. To stop, use `ctrl-c` to finish the training, and do not forget to kill the selfplay process.

```shell
python ./python/train.py --config ./config.cfg
```

## References

AlphaZero is the continuation of AlphaGo Zero, differing on certain points. In the [`materials`](./materials) folder you can find materials to understand this wonderful engine. You will also find the paper describing the UCT algorithm, implemented in this project and a [report](./materials/main.pdf) explaining in more details what has been done.

- DeepMind AlphaGo Zero official page: https://deepmind.com/blog/article/alphago-zero-starting-scratch.
- DeepMind AlphaZero official page: https://deepmind.com/blog/article/alphazero-shedding-new-light-grand-games-chess-shogi-and-go.

## Experiments

I tried to train AlphaZero on 1.8GHz dual-core Intel Core i5 CPU with a board size of 8 and with the goal of aligning 3 stones, but I did not manage to get a good network yet.

## Feel free to contact me

If you have any ideas or issues on this project, feel free to contact me at <eliekadoche78@gmail.com>. Critics will be appreciated.

## TODO

- Develop unit tests: important since nothing guarantees everything work.
- Launch a training on GPU.
