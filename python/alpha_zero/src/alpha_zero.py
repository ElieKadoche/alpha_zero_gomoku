import copy
import math

import numpy
import torch

import src


class AlphaZero:
    def __init__(self):
        # Map for total visits done per game state N(s)
        # Dictionnary <hash, int>
        self.map_states_total_visits = {}

        # Map for total visits done per action per state N(s,a)
        # Dictionnary <hash, list(int)>
        self.map_moves_total_visits = {}

        # Map for total action value per action per state W(s,a)
        # Dictionnary <hash, list(float)>
        self.map_moves_total_action_value = {}

        # Map for mean action value per action per state Q(s,a)
        # Dictionnary <hash, list(float)>
        self.map_moves_mean_action_value = {}

        # Map for prior probability per action per state P(s,a)
        # Dictionnary <hash, list(float)>
        self.map_moves_prior_proba = {}

        # Constant epsilon for the root when adding Dirichlet noise
        # P(s,a) = (1 - eps) * P(s,a) + eps * noise
        self.root_exploration_fraction = \
            src.CONFIG["root_exploration_fraction"]

        # Parameter of the Dirichlet law Dir(alpha)
        self.root_dirichlet_alpha = src.CONFIG["root_dirichlet_alpha"]

        # Exploration constant
        self.exploration_constant = src.CONFIG["az_exploration_constant"]

        # Max number of visits
        self.max_v = src.CONFIG["max_visits"]

        # Load network
        loaded = False
        while not loaded:
            try:
                self.model = torch.jit.load(src.CONFIG["net_path"])
                loaded = True
            except RuntimeError:
                pass

    def add_dirichlet_noise(self, h):
        nb_moves = len(self.map_moves_prior_proba[h])
        noises = numpy.random.gamma(self.root_dirichlet_alpha, 1, nb_moves)
        for idx in range(nb_moves):
            noise = noises[idx]
            raw_p = self.map_moves_prior_proba[h][idx]
            self.map_moves_prior_proba[h][idx] = (
                1 - self.root_exploration_fraction
            ) * raw_p + self.root_exploration_fraction * noise

    def select_move(self, game, legal_moves):
        h = game.hash
        putcs = []

        for m in range(len(legal_moves)):
            # Q(s,a), exploitation term
            q = self.map_moves_mean_action_value[h][m]

            # U(s,a), exploration term
            p = self.map_moves_prior_proba[h][m]
            ns = float(self.map_states_total_visits[h])  # N(s)
            nsa = float(self.map_moves_total_visits[h][m])  # N(s,a)
            u = self.exploration_constant * p + (math.sqrt(ns) / (1 + nsa))

            # Compute final formula
            putcs.append(q + u)

        best_move_index = putcs.index(max(putcs))
        return best_move_index

    def backup(self, last_turn, states, actions, v):
        # Since v is in [-1;1], -1 if black wins, 0 if draw and 1 if white wins
        # and since in select_move we always take the maximum
        # we need to invert v when it is black to play
        last_player = float(last_turn * -1)

        for t in reversed(range(len(states))):
            # N(s) += 1
            self.map_states_total_visits[states[t]] += 1

            # N(s,a) += 1
            self.map_moves_total_visits[states[t]][actions[t]] += 1

            # W(s,a) += / -= v
            self.map_moves_total_action_value[states[t]
                                              ][actions[t]] += last_player * v

            # Q(s,a) = W(s,a) / N(s,a)
            self.map_moves_mean_action_value[states[t]][actions[t]] = (
                self.map_moves_total_action_value[states[t]][actions[t]]
                / self.map_moves_total_visits[states[t]][actions[t]]
            )

            # As we backup, last player changes
            last_player *= -1

    def new_node(self, game, legal_moves):
        n = len(legal_moves)
        h = game.hash

        self.map_states_total_visits[h] = 1
        self.map_moves_total_visits[h] = [0 for _ in range(n)]
        self.map_moves_total_action_value[h] = [0 for _ in range(n)]
        self.map_moves_mean_action_value[h] = [0 for _ in range(n)]
        self.map_moves_prior_proba[h] = src.get_legal_prior(
            self.model, game, legal_moves
        )

    def sim_tree(self, game):
        memory_states, memory_actions = [], []
        while game.winner == 0:
            legal_moves = game.get_legal_moves()
            if game.hash not in self.map_states_total_visits:
                self.new_node(game, legal_moves)
                return memory_states, memory_actions
            move_index = self.select_move(game, legal_moves)
            memory_states.append(game.hash)
            memory_actions.append(move_index)
            game.play_move(legal_moves[move_index])
        return memory_states, memory_actions

    def run_mcts(self, game):
        # If the node does not exist, create it
        if game.hash not in self.map_states_total_visits:
            self.new_node(game, game.get_legal_moves())
        self.add_dirichlet_noise(game.hash)

        # While we can
        for visit in range(self.max_v):
            game_bis = copy.deepcopy(game)
            memory_states, memory_actions = self.sim_tree(game_bis)
            _, v = self.model(torch.unsqueeze(game_bis.state, 0))
            self.backup(game_bis.turn, memory_states, memory_actions, v[0])

    def play(self, game):
        # AlphaZero thinking
        self.run_mcts(game)

        # Play the move with the highest visits
        total_visits = self.map_moves_total_visits[game.hash]
        best_move_index = total_visits.index(max(total_visits))
        move = game.get_legal_moves()[best_move_index]
        game.play_move(move)
