import numpy as np
import torch

import src


class Game:
    def __init__(self):
        self.b = src.CONFIG["board_size"]
        self.h = src.CONFIG["history_planes"]
        self.a = src.CONFIG["nb_aligned"]

        self.turn = -1  # Black always begins
        self.hash = 0  # Hash for empty board
        self.winner = 0
        self.moves_nb = 0

        # Tensor representation
        device = src.CONFIG["device"]
        current_state = torch.zeros((2 * self.h, self.b, self.b)).to(device)
        player_plane = torch.ones((1, self.b, self.b)).to(device)
        self.state = torch.cat((current_state, player_plane), 0)

        # Empty board
        self.board = np.zeros((self.b, self.b))

    def is_move_valid(self, move):
        # Not valid if not player's turn
        if move[0] != self.turn:
            return False

        # Not valid if outside the board
        if move[1] < 0 or move[1] >= self.b:
            return False

        if move[2] < 0 or move[2] >= self.b:
            return False

        # Not valid if there is already a stone
        if self.board[move[1]][move[2]] != 0:
            return False

        return True

    def get_legal_moves(self):
        legal_moves = []
        for row in range(self.b):
            for col in range(self.b):
                if self.is_move_valid((self.turn, row, col)):
                    legal_moves.append((self.turn, row, col))
        return legal_moves

    def play_move(self, move):
        # Play the move
        self.board[move[1]][move[2]] = move[0]

        # Update winner
        self.winner = self.update_winner(move)

        # Update hash
        self.hash = src.get_next_hash(self.hash, move)

        # Change turn
        self.moves_nb += 1
        self.turn = move[0] * -1

        # Update tensor
        self.update_tensor(move)

    def update_winner(self, last_move):
        # Horizontal check
        CA = max(0, last_move[2] - self.a + 1)  # Column start
        CB = min(last_move[2] + self.a - 1, self.b - 1)
        nb_aligned = 0
        for col in range(CA, CB + 1):
            if self.board[last_move[1]][col] == last_move[0]:
                nb_aligned += 1
            else:
                nb_aligned = 0
            if nb_aligned == self.a:
                return last_move[0]

        # Vertical check
        RA = max(0, last_move[1] - self.a + 1)  # Row start
        RB = min(last_move[1] + self.a - 1, self.b - 1)
        nb_aligned = 0
        for row in range(RA, RB + 1):
            if self.board[row][last_move[2]] == last_move[0]:
                nb_aligned += 1
            else:
                nb_aligned = 0
            if nb_aligned == self.a:
                return last_move[0]

        # Steps for diagonals
        DUL = min(last_move[1] - RA, last_move[2] - CA)  # Diagonal upper left
        # Diagonal bottom right
        DBR = min(RB - last_move[1], CB - last_move[2])
        DBL = min(RB - last_move[1], last_move[2] - CA)  # Diagonal bottom left
        DUR = min(last_move[1] - RA, CB - last_move[2])  # Diagonal upper right

        # Diagonal check (up to bottom)
        nb_aligned = 0
        for s in range(0, DUL + DBR + 1):
            if (self.board[last_move[1] - DUL + s][last_move[2] - DUL + s]
                    == last_move[0]):
                nb_aligned += 1
            else:
                nb_aligned = 0
            if nb_aligned == self.a:
                return last_move[0]

        # Diagonal check (bottom to up)
        nb_aligned = 0
        for s in range(0, DBL + DUR + 1):
            if (self.board[last_move[1] + DBL - s][last_move[2] - DBL + s]
                    == last_move[0]):
                nb_aligned += 1
            else:
                nb_aligned = 0
            if nb_aligned == self.a:
                return last_move[0]

        # No more move to play
        if self.moves_nb > self.b ** 2:
            return 2

        return 0

    def update_tensor(self, last_move):
        device = src.CONFIG["device"]

        # Player plane
        player_plane = torch.ones((1, self.b, self.b)).to(device)
        if self.turn == -1:
            player_plane = torch.zeros((1, self.b, self.b)).to(device)

        # Current state planes
        player_current_plane = self.state.select(0, 1).clone().to(device)
        opponent_current_plane = self.state.select(0, 0).clone().to(device)
        opponent_current_plane[last_move[1]][last_move[2]] = 1.0
        player_current_plane = torch.unsqueeze(player_current_plane, 0)
        opponent_current_plane = torch.unsqueeze(opponent_current_plane, 0)

        # Previous planes
        state = self.state.narrow(0, 0, self.h * 2 - 2).clone().to(device)

        # We invert previous planes because perspective is from current player
        previous_planes = torch.cat((
            torch.unsqueeze(state.select(0, 1), 0),
            torch.unsqueeze(state.select(0, 0), 0)), 0
        )
        for i in range(2, state.size()[0], 2):
            previous_planes = torch.cat((
                previous_planes,
                torch.unsqueeze(state.select(0, i + 1), 0),
                torch.unsqueeze(state.select(0, i), 0)), 0
            )

        # We concatenate everything
        self.state = torch.cat((
            player_current_plane,
            opponent_current_plane,
            previous_planes,
            player_plane), 0
        )
