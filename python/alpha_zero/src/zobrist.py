import random

ZOBRIST = {}


def init_zobrist_hashes(board_size):
    for row in range(board_size):
        for col in range(board_size):
            ZOBRIST[(-1, row, col)] = random.getrandbits(64)
            ZOBRIST[(1, row, col)] = random.getrandbits(64)


def get_next_hash(previous_hash, move):
    return previous_hash ^ ZOBRIST[move]
