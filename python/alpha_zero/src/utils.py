import os

import torch

import src


def save_tensor_to_file(tensor, path, buffer_size):
    new_tensor = tensor.to(src.CONFIG["device"])
    if os.path.exists(path):
        # Previous tensor
        previous_tensor = torch.load(path).to(src.CONFIG["device"])

        # Compute new tensor
        new_tensor = torch.cat((previous_tensor, tensor),
                               0).to(src.CONFIG["device"])

    # Check if size is smaller than buffer size
    buffer_current_size = new_tensor.size()[0]
    if buffer_current_size > buffer_size:
        # Removed first elements
        new_tensor = new_tensor.narrow(
            0,
            buffer_current_size - buffer_size,
            buffer_size
        ).to(src.CONFIG["device"])

    # Save new tensor
    torch.save(new_tensor, path)


def get_legal_prior(model, game, legal_moves):
    policy, _ = model(torch.unsqueeze(game.state, 0))
    legal_prior = []

    for m in range(len(legal_moves)):
        prior = policy[0][game.b * legal_moves[m][1] + legal_moves[m][2]]
        legal_prior.append(prior)

    return legal_prior
