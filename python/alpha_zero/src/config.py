import torch

CONFIG = {}


def init_config(config_file):
    # Game
    CONFIG["board_size"] = int(config_file["cfg"]["board_size"])
    CONFIG["nb_aligned"] = int(config_file["cfg"]["nb_aligned"])

    # Cuda
    use_cuda = bool(config_file["cfg"]["use_cuda"])
    use_cuda = use_cuda and torch.cuda.is_available()
    CONFIG["device"] = torch.device("cuda" if use_cuda else "cpu")

    # AlphaZero
    CONFIG["root_exploration_fraction"] = \
        float(config_file["cfg"]["root_exploration_fraction"])
    CONFIG["az_exploration_constant"] = \
        float(config_file["cfg"]["az_exploration_constant"])
    CONFIG["max_visits"] = int(config_file["cfg"]["max_visits"])
    CONFIG["root_dirichlet_alpha"] = float(
        config_file["cfg"]["root_dirichlet_alpha"])

    # Net
    CONFIG["net_path"] = config_file["cfg"]["net_path"]
    CONFIG["history_planes"] = int(config_file["cfg"]["history_planes"])

    # Selfplay
    CONFIG["buffer_size"] = int(config_file["cfg"]["buffer_size"])
    CONFIG["selfplay_dir"] = config_file["cfg"]["selfplay_dir"]
    CONFIG["nb_jobs"] = int(config_file["cfg"]["nb_jobs"])
