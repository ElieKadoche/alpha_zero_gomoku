import argparse
import configparser
import threading

import torch

import src

LOCK = threading.Lock()


def selfplay(thread_index):
    board_size = src.CONFIG["board_size"]
    buffer_size = src.CONFIG["buffer_size"]
    device = src.CONFIG["device"]

    x_vector, p_vector, v_vector = [], [], []

    while True:
        game = src.Game()
        az_engine = src.AlphaZero()

        while game.winner == 0:
            # X TENSOR
            x_vector.append(game.state)

            # Get hash and legal moves
            current_hash = game.hash
            legal_moves = game.get_legal_moves()

            # AlphaZero thinking and playing
            az_engine.play(game)

            # Computing the policy of the network
            state_visits = az_engine.map_states_total_visits[current_hash]
            move_visits = az_engine.map_moves_total_visits[current_hash]
            current_p = torch.zeros((board_size ** 2)).to(device)
            for m in range(len(move_visits)):
                index = board_size * legal_moves[m][1] + legal_moves[m][2]
                current_p[index] = move_visits[m] / state_visits

            # P TENSOR
            p_vector.append(current_p)

        # V TENSOR
        value_value = game.winner
        if game.winner == 2:
            value_value = 0  # Draw
        current_v = torch.full((game.moves_nb, 1), value_value).to(device)
        v_vector.append(current_v)

        # Lock files and save tensors
        LOCK.acquire()
        selfplay_dir_x = "{}/x.pt".format(src.CONFIG["selfplay_dir"])
        selfplay_dir_p = "{}/p.pt".format(src.CONFIG["selfplay_dir"])
        selfplay_dir_v = "{}/v.pt".format(src.CONFIG["selfplay_dir"])

        src.save_tensor_to_file(torch.stack(
            x_vector, 0), selfplay_dir_x, buffer_size)
        src.save_tensor_to_file(torch.stack(
            p_vector, 0), selfplay_dir_p, buffer_size)
        src.save_tensor_to_file(torch.cat(v_vector, 0),
                                selfplay_dir_v, buffer_size)
        LOCK.release()

        # Empty vectors of current tensors
        x_vector.clear()
        p_vector.clear()
        v_vector.clear()


if __name__ == "__main__":
    # Parser
    # ---------------
    parser = argparse.ArgumentParser()
    parser.add_argument("--config", help="path",
                        default="../../config.cfg", type=str)
    args = parser.parse_args()

    # Config file
    # ---------------
    with open(args.config) as f:
        file_content = "[cfg]\n" + f.read()
    config_file = configparser.RawConfigParser()
    config_file.read_string(file_content)

    # Init config and zobrist
    # ---------------
    src.init_config(config_file)
    src.init_zobrist_hashes(src.CONFIG["board_size"])

    # Launch threads
    # ---------------
    jobs = []
    for thread_index in range(int(src.CONFIG["nb_jobs"])):
        t = threading.Thread(target=selfplay, args=(thread_index,))
        jobs.append(t)
        t.start()

    for job in jobs:
        job.join()
