import argparse
import configparser
import os
import signal
import time

import torch
import torch.nn as nn
import torch.utils.data as data
from tqdm import tqdm


class AZ_loss(nn.Module):
    def __init__(self):
        super(AZ_loss, self).__init__()

    def forward(self, out_p, out_v, p, v):
        value_loss = torch.mean(torch.pow(out_v - v, 2))
        policy_loss = -1 * torch.mean(torch.sum(p * torch.log10(out_p), 1))
        return value_loss + policy_loss


class SelfPlayDataset(data.dataset.Dataset):
    def __init__(self, x_path, p_path, v_path):

        files_loaded = False

        # Since C++/Python writes in those files, the loading might
        # raise errors, but this case may be rare. Python musts load
        # the files when C++/Python is not writting in them

        # In libtorch 1.4, serialization only supports module holders
        # so we need to load tensor differently depending on whether
        # Python or C++ generates selfplay data

        while not files_loaded:
            try:
                # For Python
                self.x = torch.load(x_path)
                self.p = torch.load(p_path)
                self.v = torch.load(v_path)
                files_loaded = True
            except (KeyboardInterrupt, SystemExit):
                raise
            except:
                pass

            try:
                # For C++
                x_jit_module = torch.jit.load(x_path)
                p_jit_module = torch.jit.load(p_path)
                v_jit_module = torch.jit.load(v_path)
                self.x = next(x_jit_module.parameters())
                self.p = next(p_jit_module.parameters())
                self.v = next(v_jit_module.parameters())
                files_loaded = True
            except (KeyboardInterrupt, SystemExit):
                raise
            except:
                pass

    def __getitem__(self, index):
        return self.x[index], self.p[index], self.v[index]

    def __len__(self):
        # Same here, to avoid any problems, we do this verification
        size = min(len(self.x), len(self.p), len(self.v))
        return size


if __name__ == "__main__":
    # Parser
    # ---------------
    parser = argparse.ArgumentParser()
    parser.add_argument("--config", help="path",
                        default="../config.cfg", type=str)
    args = parser.parse_args()

    # Configuration file
    # ---------------
    with open(args.config) as f:
        file_content = "[cfg]\n" + f.read()
    config = configparser.RawConfigParser()
    config.read_string(file_content)

    # Cuda configuration
    # ---------------
    use_cuda = config["cfg"]["use_cuda"]
    use_cuda = use_cuda and torch.cuda.is_available()
    device = torch.device("cuda" if use_cuda else "cpu")

    # Model
    # ---------------
    model = torch.jit.load(config["cfg"]["net_path"]).to(device)
    lock_file = "{}.lock".format(config["cfg"]["net_path"])

    # Hyper parameters
    # ---------------
    batch_size = int(config["cfg"]["batch_size"])
    l2_regularization = float(config["cfg"]["l2_regularization"])

    # Training
    # ---------------
    model.train()
    az_loss = AZ_loss()
    learning_rate = float(config["cfg"]["learning_rate"])
    optimizer = torch.optim.SGD(
        model.parameters(),
        lr=learning_rate,
        momentum=0.9,
        weight_decay=l2_regularization,
    )

    # optimizer = torch.optim.Adam(
    #     model.parameters(),
    #     lr=learning_rate,
    #     weight_decay=l2_regularization
    # )

    # Data
    # ---------------
    selfplay_dir = config["cfg"]["selfplay_dir"]
    x_path = "{}/x.pt".format(selfplay_dir)
    p_path = "{}/p.pt".format(selfplay_dir)
    v_path = "{}/v.pt".format(selfplay_dir)

    # Wait until data is available
    while not os.path.exists(x_path) \
            or not os.path.exists(p_path) \
            or not os.path.exists(v_path):
        time.sleep(5)

    epoch = 0
    while True:

        # Dataset
        # ---------------
        selfplay_dataset = SelfPlayDataset(x_path, p_path, v_path)
        data_loader = data.DataLoader(
            selfplay_dataset,
            batch_size=batch_size,
            shuffle=True
        )

        last_batch_index = len(selfplay_dataset) // batch_size
        if len(selfplay_dataset) % batch_size == 0:
            last_batch_index -= 1

        # scheduler = torch.optim.lr_scheduler.StepLR(
        #     optimizer,
        #     step_size=50,
        #     gamma=0.1,
        #     last_epoch=-1
        # )

        running_loss = 0.0

        t = tqdm(
            enumerate(data_loader),
            desc="Epoch {:09d} - Batch loss = ---".format(epoch + 1),
            total=len(selfplay_dataset) // batch_size,
            unit="batch",
        )
        for i_batch, batch in t:
            x, p, v = batch[0].to(device), batch[1].to(
                device), batch[2].to(device)

            optimizer.zero_grad()
            out_p, out_v = model(x)
            loss = az_loss(out_p, out_v, p, v)
            running_loss += loss.item()
            t.set_description("Epoch {:09d} - Batch loss = {:.9f}".format(
                epoch + 1, loss.item()))
            loss.backward()
            optimizer.step()

            if i_batch == last_batch_index:
                t.set_postfix_str("mean_total_loss = {:.9f}".format(
                    running_loss / (last_batch_index + 1)))

        epoch += 1

        # scheduler.step()

        # Saving
        # ---------------

        # When saving the net, we the programm can not be interrupted
        s = signal.signal(signal.SIGINT, signal.SIG_IGN)

        # We create a lock file to let C++ know that it can not open it
        lock = open(lock_file, "w+")
        time.sleep(1)  # For security
        model.save(config["cfg"]["net_path"])
        lock.close()

        # Back to default
        os.remove(lock_file)
        signal.signal(signal.SIGINT, s)
