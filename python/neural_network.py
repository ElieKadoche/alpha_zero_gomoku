import argparse
import configparser

import torch
import torch.nn as nn
import torch.nn.functional as F
from torchsummary import summary


class NeuralNetwork(nn.Module):
    def __init__(self, config):
        super(NeuralNetwork, self).__init__()

        # Inputs
        # ---------------
        self.n = int(config["cfg"]["board_size"])
        self.t = int(config["cfg"]["history_planes"])
        self.f = self.t * 2 + 1  # Number of features

        # Outputs
        # ---------------
        self.policy_out_classes = self.n ** 2  # +1 for pass
        self.policy_activation = nn.Softmax(dim=1)
        self.value_nb_hidden = int(config["cfg"]["value_nb_hidden"])

        # Hyper parameters
        # ---------------
        self.b = False  # Bias
        self.blocks = int(config["cfg"]["blocks"])
        self.c = int(config["cfg"]["filters"])

        # Convolution layers
        # ---------------
        self.conv_input = nn.Conv2d(self.f, self.c, (3, 3), 1, 1, bias=self.b)
        self.conv_main = nn.Conv2d(self.c, self.c, (3, 3), 1, 1, bias=self.b)
        self.conv_policy = nn.Conv2d(self.c, 2, (1, 1), 1, 0, bias=self.b)
        self.conv_value = nn.Conv2d(self.c, 1, (1, 1), 1, 0, bias=self.b)

        # Dense layers
        # ---------------
        self.dense_policy = nn.Linear(
            2 * (self.n ** 2), self.policy_out_classes, self.b)
        self.dense_value_0 = nn.Linear(
            1 * (self.n ** 2), self.value_nb_hidden, self.b)
        self.dense_value_1 = nn.Linear(self.value_nb_hidden, 1, self.b)

        # Normalization layers
        # ---------------
        self.batch_norm_default = nn.BatchNorm2d(self.c)
        self.batch_norm_policy = nn.BatchNorm2d(2)
        self.batch_norm_value = nn.BatchNorm2d(1)

    def forward(self, x):
        x = self.conv_input(x)
        x = self.batch_norm_default(x)
        x = F.relu(x)

        for _ in range(self.blocks):
            residual = x
            x = self.conv_main(x)
            x = self.batch_norm_default(x)
            x = F.relu(x)
            x = self.conv_main(x)
            x = self.batch_norm_default(x)
            x = x + residual
            x = F.relu(x)

        p = self.conv_policy(x)
        p = self.batch_norm_policy(p)
        p = F.relu(p)
        p = torch.flatten(p, 1, -1)
        p = self.dense_policy(p)
        p = self.policy_activation(p)

        v = self.conv_value(x)
        v = self.batch_norm_value(v)
        v = F.relu(v)
        v = torch.flatten(v, 1, -1)
        v = self.dense_value_0(v)
        v = F.relu(v)
        v = self.dense_value_1(v)
        v = torch.tanh(v)

        return p, v


if __name__ == "__main__":
    # Parser
    # ---------------
    parser = argparse.ArgumentParser()
    parser.add_argument("--config", help="path",
                        default="../config.cfg", type=str)
    args = parser.parse_args()

    # Configuration file
    # ---------------
    with open(args.config) as f:
        file_content = "[cfg]\n" + f.read()
    config = configparser.RawConfigParser()
    config.read_string(file_content)

    # Cuda configuration
    # ---------------
    use_cuda = config["cfg"]["use_cuda"]
    use_cuda = use_cuda and torch.cuda.is_available()
    device = torch.device("cuda" if use_cuda else "cpu")

    # Model creation
    # ---------------
    model = NeuralNetwork(config).to(device)
    # summary(model, (channels, width, heigth))

    # Converting to Torch Script
    # ---------------
    script_module = torch.jit.script(model)
    script_module.save(config["cfg"]["net_path"])
